#!/usr/bin/env bash

set -evx

# Example:
# vendor/smtm/smtm-email/bin/test-sendgrid.sh "XX.XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" "noreply@smtm.com" "Smtm" "test@example.com" "Example Test" "Hello, World!" "Heya!"
# bin/test-sendgrid.sh "XX.XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" "noreply@smtm.com" "Smtm" "test@example.com" "Example Test" "Hello, World!" "Heya!"

TOKEN="$1"
FROM_EMAIL="$2"
FROM_NAME="$3"
TO_EMAIL="$4"
TO_NAME="$5"
SUBJECT="$6"
BODY="$7"

curl --request POST --url 'https://api.sendgrid.com/v3/mail/send' --header 'Authorization: Bearer '${TOKEN} --header 'Content-Type: application/json' --data '{"personalizations":[{"to":[{"email":"'"${TO_EMAIL}"'","name":"'"${TO_NAME}"'"}],"subject":"'"${SUBJECT}"'"}],"content": [{"type": "text/plain", "value": "'"${BODY}"'"}],"from":{"email":"'"${FROM_EMAIL}"'","name":"'"${FROM_NAME}"'"}}'
