<?php

namespace Smtm\Email\Infrastructure\Service;

use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use JetBrains\PhpStorm\Pure;
use Laminas\Mail\Address;
use Laminas\Mail\AddressList;
use Laminas\Mail\Message;
use Traversable;

class EmailRecipientDomainFilter extends AbstractInfrastructureService
{
    /**
     * @param InfrastructureServicePluginManager $infrastructureServicePluginManager
     * @param array $config
     */
    #[Pure] public function __construct(
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected array $config
    ) {
        parent::__construct($infrastructureServicePluginManager);
    }

    /**
     * @return bool
     */
    public function isConfigurationPresent(): bool
    {
        return isset($this->config['allowedNonProductionDomains']) &&
            count(array_filter($this->config['allowedNonProductionDomains'])) > 0;
    }

    /**
     * @param Message $message
     */
    public function filter(Message $message): void
    {
        $message->setTo($this->filterRecipientList($message->getTo()));
        $message->setCc($this->filterRecipientList($message->getCc()));
        $message->setBcc($this->filterRecipientList($message->getBcc()));
    }

    /**
     * @param Message $message
     * @return bool
     */
    public function shouldBeSent(Message $message): bool
    {
        return $message->getTo()->count() > 0;
    }


    /**
     * @param AddressList|Address|Traversable|array $addressList
     * @return AddressList
     */
    private function filterRecipientList(AddressList|Address|Traversable|array $addressList): AddressList
    {
        $clonedAddressList = new AddressList();

        if ($addressList instanceof Traversable) {
            while ($address = $addressList->current()) {
                if ($this->isAllowedNonProductionEmail($address->getEmail())) {
                    $clonedAddressList->add($address);
                }

                $addressList->next();
            }
        } else if (is_array($addressList)) {
            foreach ($addressList as $address) {
                if ($this->isAllowedNonProductionEmail($address->getEmail())) {
                    $clonedAddressList->add($address);
                }
            }
        } else if ($addressList instanceof Address && $this->isAllowedNonProductionEmail($addressList->getEmail())) {
            $clonedAddressList->add($addressList);
        }

        return $clonedAddressList;
    }

    /**
     * @param string $emailAddress
     * @return bool
     */
    private function isAllowedNonProductionEmail(string $emailAddress): bool
    {
        list ($user, $domain) = explode('@', $emailAddress);

        return in_array($domain, $this->config['allowedNonProductionDomains']);
    }
}

