<?php

declare(strict_types=1);

namespace Smtm\Email\Infrastructure\Service;

use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use JetBrains\PhpStorm\Pure;
use Laminas\Mail\Address;
use Laminas\Mail\AddressList;
use Laminas\Mail\Message;
use Laminas\Mail\Transport\TransportInterface;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mime\Mime;
use Laminas\Mime\Part as MimePart;
use Traversable;

class EmailService extends AbstractInfrastructureService
{
    #[Pure] public function __construct(
        InfrastructureServicePluginManager   $infrastructureServicePluginManager,
        protected TransportInterface         $transport,
        protected EmailRecipientDomainFilter $nonProductionEmailFilter,
        protected array                      $config
    ) {
        parent::__construct($infrastructureServicePluginManager);
    }

    public function send(
        \Laminas\Mime\Message|string|null $content = null,
        ?string $subject = null,
        AddressList|Address|iterable $from = [],
        AddressList|Address|iterable $replyTo = [],
        AddressList|Address|iterable $to = [],
        AddressList|Address|iterable $cc = [],
        AddressList|Address|iterable $bcc = []
    ): void {
        $message = new Message();

        $message
            ->setBody($content)
            ->setSubject($subject ?? '')
            ->setFrom($from)
            ->setReplyTo($replyTo)
            ->setTo($to)
            ->setCc($cc)
            ->setBcc($bcc);

        /** Check if non production email should be sent out and filter the recipient list */
        if ($this->nonProductionEmailFilter->isConfigurationPresent()) {
            $this->nonProductionEmailFilter->filter($message);

            if (!$this->nonProductionEmailFilter->shouldBeSent($message)) {
                return;
            }
        }

        $this->transport->send($message);
    }

    private function filterRecipientList(AddressList|Address|iterable $addressList): AddressList
    {
        $clonedAddressList = new AddressList();
        $clonedAddressList->addMany($addressList);

        foreach ($clonedAddressList as $address) {
            if (!$this->isAllowedNonProductionEmail($address->getEmail())) {
                $clonedAddressList->delete($address->getEmail());
            }
        }

        return $clonedAddressList;
    }
}
