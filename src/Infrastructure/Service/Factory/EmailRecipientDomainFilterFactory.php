<?php

namespace Smtm\Email\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

class EmailRecipientDomainFilterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(InfrastructureServicePluginManager::class),
            $container->get('config')['email']
        );
    }
}
