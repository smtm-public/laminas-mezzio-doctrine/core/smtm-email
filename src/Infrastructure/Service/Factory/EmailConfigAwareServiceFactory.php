<?php

declare(strict_types=1);

namespace Smtm\Email\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Email\Infrastructure\Service\EmailRecipientDomainFilter;
use Laminas\Mail\Transport\Exception\InvalidArgumentException;
use Laminas\Mail\Transport\Smtp;
use Laminas\Mail\Transport\SmtpOptions;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EmailConfigAwareServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $options = match ($container->get('config')['email']['emailProcessor']['transport']) {
            Smtp::class => new SmtpOptions(
                (function (array $optionsArray): array {
                    if ($optionsArray['connectionClass'] === '') {
                        unset($optionsArray['connectionClass']);
                    }

                    if ($optionsArray['connectionConfig'] === '') {
                        unset($optionsArray['connectionConfig']);
                    } else {
                        $optionsArray['connectionConfig'] = json_decode($optionsArray['connectionConfig'], true);
                    }

                    if ($optionsArray['name'] === '') {
                        unset($optionsArray['name']);
                    }

                    if ($optionsArray['host'] === '') {
                        unset($optionsArray['host']);
                    }

                    return $optionsArray;
                }) (
                    $container->get('config')['email']['emailProcessor'][
                        $container->get('config')['email']['emailProcessor']['transport']
                    ]
                )
            ),
            default => throw new InvalidArgumentException('Invalid emailProcessor transport'),
        };

        return new $requestedName(
            $container->get(InfrastructureServicePluginManager::class),
            new ($container->get('config')['email']['emailProcessor']['transport'])($options),
            $container->get(InfrastructureServicePluginManager::class)->get(EmailRecipientDomainFilter::class),
            $container->get('config')['email']
        );
    }
}
