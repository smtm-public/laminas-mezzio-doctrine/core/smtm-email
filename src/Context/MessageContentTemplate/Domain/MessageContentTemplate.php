<?php

declare(strict_types=1);

namespace Smtm\Email\Context\MessageContentTemplate\Domain;

use Smtm\Base\Domain\AbstractUuidAwareEntity;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageContentTemplate extends AbstractUuidAwareEntity implements NotArchivedAwareEntityInterface, ArchivedDateTimeAwareEntityInterface
{

    use NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;

    protected string $name;
    protected string $contentType;
    protected ?string $template = null;
    protected ?string $fileTemplate = null;
    protected array $params = [];

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function setContentType(string $contentType): static
    {
        $this->contentType = $contentType;

        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(?string $template): static
    {
        $this->template = $template;

        return $this;
    }

    public function getFileTemplate(): ?string
    {
        return $this->fileTemplate;
    }

    public function setFileTemplate(?string $fileTemplate): static
    {
        $this->fileTemplate = $fileTemplate;

        return $this;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): static
    {
        $this->params = $params;

        return $this;
    }
}
