<?php

declare(strict_types=1);

namespace Smtm\Email\Context\MessageContentTemplate\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface MessageContentTemplateRepositoryInterface extends RepositoryInterface
{

}
