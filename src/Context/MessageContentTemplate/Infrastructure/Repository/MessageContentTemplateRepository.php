<?php

declare(strict_types=1);

namespace Smtm\Email\Context\MessageContentTemplate\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageContentTemplateRepository extends AbstractRepository implements MessageContentTemplateRepositoryInterface
{

}
