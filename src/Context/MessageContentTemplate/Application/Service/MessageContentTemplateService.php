<?php

declare(strict_types=1);

namespace Smtm\Email\Context\MessageContentTemplate\Application\Service;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\ArchivableUuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Email\Context\MessageContentTemplate\Domain\MessageContentTemplate;
use Doctrine\Persistence\ManagerRegistry;
use JetBrains\PhpStorm\Pure;
use Mezzio\Template\TemplateRendererInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageContentTemplateService extends AbstractDbService implements UuidAwareEntityDbServiceInterface
{

    use ArchivableUuidAwareEntityDbServiceTrait, UuidAwareEntityDbServiceTrait;

    protected ?string $domainObjectName = MessageContentTemplate::class;
    protected ?string $hydratorName = null;

    #[Pure] public function __construct(
        ApplicationServicePluginManager $applicationServicePluginManager,
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        HydratorPluginManager $hydratorPluginManager,
        ExtractorPluginManager $extractorPluginManager,
        \Laminas\Hydrator\HydratorPluginManager $baseHydratorPluginManager,
        ManagerRegistryInterface $entityManagerRegistry,
        protected ?TemplateRendererInterface $template = null
    ) {
        parent::__construct(
            $applicationServicePluginManager,
            $infrastructureServicePluginManager,
            $hydratorPluginManager,
            $extractorPluginManager,
            $baseHydratorPluginManager,
            $entityManagerRegistry
        );
    }

    public function renderTemplate(
        MessageContentTemplate $messageContentTemplate
    ): string {
        if ($messageContentTemplate->getFileTemplate() !== null) {
            return $this->template->render(
                $messageContentTemplate->getFileTemplate(),
                $messageContentTemplate->getParams()
            );
        }

        return strtr($messageContentTemplate->getTemplate(), $messageContentTemplate->getParams());
    }
}
