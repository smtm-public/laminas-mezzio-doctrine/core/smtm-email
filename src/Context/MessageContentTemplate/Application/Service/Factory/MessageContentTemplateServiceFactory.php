<?php

declare(strict_types=1);

namespace Smtm\Email\Context\MessageContentTemplate\Application\Service\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Email\Context\MessageContentTemplate\Application\Service\MessageContentTemplateService;
use Doctrine\Persistence\ManagerRegistry;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
final class MessageContentTemplateServiceFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): MessageContentTemplateService {
        return new MessageContentTemplateService(
            $container->get(ApplicationServicePluginManager::class),
            $container->get(InfrastructureServicePluginManager::class),
            $container->get(HydratorPluginManager::class),
            $container->get(ExtractorPluginManager::class),
            $container->get(\Laminas\Hydrator\HydratorPluginManager::class),
            $container
                ->get(InfrastructureServicePluginManager::class)
                ->get(ManagerRegistryInterface::class),
            $container->get(TemplateRendererInterface::class)
        );
    }
}
