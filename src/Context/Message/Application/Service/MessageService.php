<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\ArchivableUuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Email\Context\Message\Application\Hydrator\MessageHydrator;
use Smtm\Email\Context\Message\Context\MessageAttachment\Application\Service\MessageAttachmentService;
use Smtm\Email\Context\Message\Context\MessageAttachment\Domain\MessageAttachment;
use Smtm\Email\Context\Message\Context\MessageBcc\Application\Service\MessageBccService;
use Smtm\Email\Context\Message\Context\MessageBcc\Domain\MessageBcc;
use Smtm\Email\Context\Message\Context\MessageCc\Application\Service\MessageCcService;
use Smtm\Email\Context\Message\Context\MessageCc\Domain\MessageCc;
use Smtm\Email\Context\Message\Context\MessageContent\Application\Service\MessageContentService;
use Smtm\Email\Context\Message\Context\MessageContent\Domain\MessageContent;
use Smtm\Email\Context\Message\Context\MessageFrom\Application\Service\MessageFromService;
use Smtm\Email\Context\Message\Context\MessageFrom\Domain\MessageFrom;
use Smtm\Email\Context\Message\Context\MessageReplyTo\Application\Service\MessageReplyToService;
use Smtm\Email\Context\Message\Context\MessageReplyTo\Domain\MessageReplyTo;
use Smtm\Email\Context\Message\Context\MessageTo\Application\Service\MessageToService;
use Smtm\Email\Context\Message\Context\MessageTo\Domain\MessageTo;
use Smtm\Email\Context\Message\Domain\Message;
use Smtm\Email\Context\MessageContentTemplate\Application\Service\MessageContentTemplateService;
use Smtm\Email\Context\MessageContentTemplate\Domain\MessageContentTemplate;
use Smtm\Email\Context\MessageSubjectTemplate\Application\Service\MessageSubjectTemplateService;
use Smtm\Email\Context\MessageSubjectTemplate\Domain\MessageSubjectTemplate;
use Smtm\Email\Infrastructure\Service\EmailService;
use Laminas\Mail\Address as LaminasAddress;
use Laminas\Mail\AddressList as LaminasAddressList;
use Laminas\Mime\Message as LaminasMessage;
use Laminas\Mime\Part as LaminasMessagePart;
use Psr\Http\Message\StreamInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageService extends AbstractDbService implements UuidAwareEntityDbServiceInterface
{

    use ArchivableUuidAwareEntityDbServiceTrait, UuidAwareEntityDbServiceTrait;

    protected ?string $domainObjectName = Message::class;
    protected ?string $hydratorName = MessageHydrator::class;

    public function setSubjectTemplate(
        Message $message,
        string $subjectTemplateName,
        array $params = []
    ): Message {
        /** @var MessageSubjectTemplateService $messageSubjectTemplateService */
        $messageSubjectTemplateService = $this->applicationServicePluginManager->get(
            MessageSubjectTemplateService::class
        );
        /** @var MessageSubjectTemplate $messageSubjectTemplate */
        $messageSubjectTemplate = $messageSubjectTemplateService->getOneBy(
            [
                'name' => $subjectTemplateName,
            ]
        );
        $messageSubjectTemplate->setParams($params);
        $message->setMessageSubjectTemplate($messageSubjectTemplate);
        $message->setSubject(
            $messageSubjectTemplateService->renderTemplate($messageSubjectTemplate)
        );

        return $message;
    }

    public function addContentWithTemplate(
        Message $message,
        string $contentTemplateName,
        string $contentType,
        array $params = []
    ): Message {
        /** @var MessageContentTemplateService $messageContentTemplateService */
        $messageContentTemplateService = $this->applicationServicePluginManager->get(
            MessageContentTemplateService::class
        );
        /** @var MessageContentTemplate $messageContentTemplate */
        $messageContentTemplate = $messageContentTemplateService->getOneBy(
            [
                'name' => $contentTemplateName,
                'contentType' => $contentType,
            ]
        );
        $messageContentTemplate->setParams($params);
        /** @var MessageContentService $messageContentService */
        $messageContentService = $this->applicationServicePluginManager->get(
            MessageContentService::class
        );
        /** @var MessageContent $messageContent */
        $messageContent = $messageContentService->prepareDomainObject();
        $messageContent->setMessageContentTemplate($messageContentTemplate);
        $messageContent->setContentType($contentType);
        $messageContent->setContent(
            $messageContentTemplateService->renderTemplate($messageContentTemplate)
        );
        $message->addMessageContent($messageContent);

        return $message;
    }

    public function addContent(
        Message $message,
        string $contentType,
        mixed $content
    ): Message {
        /** @var MessageContentService $messageContentService */
        $messageContentService = $this->applicationServicePluginManager->get(
            MessageContentService::class
        );
        /** @var MessageContent $messageContent */
        $messageContent = $messageContentService->prepareDomainObject();
        $messageContent->setContentType($contentType);

        if ($content instanceof StreamInterface) {
            $messageContent->setContent($content->getContents());
        } elseif (is_string($content)) {
            $messageContent->setContent($content);
        } elseif (is_resource($content) && get_resource_type($content)) {
            $messageContent->setContent(stream_get_contents($content));
        }

        $message->addMessageContent($messageContent);

        return $message;
    }

    public function addAttachment(
        Message $message,
        string $name,
        string $contentType,
        mixed $content
    ): Message {
        /** @var MessageAttachmentService $messageAttachmentService */
        $messageAttachmentService = $this->applicationServicePluginManager->get(
            MessageAttachmentService::class
        );
        /** @var MessageAttachment $messageAttachment */
        $messageAttachment = $messageAttachmentService->prepareDomainObject();
        $messageAttachment->setName($name);
        $messageAttachment->setContentType($contentType);

        if ($content instanceof StreamInterface) {
            $messageAttachment->setContent($content->getContents());
        } elseif (is_string($content)) {
            $messageAttachment->setContent($content);
        } elseif (is_resource($content) && get_resource_type($content)) {
            $messageAttachment->setContent(stream_get_contents($content));
        }

        $message->addMessageAttachment($messageAttachment);

        return $message;
    }

    public function addFrom(
        Message $message,
        string $address,
        ?string $name = null
    ): Message {
        /** @var MessageFromService $messageFromService */
        $messageFromService = $this->applicationServicePluginManager->get(
            MessageFromService::class
        );
        /** @var MessageFrom $messageFrom */
        $messageFrom = $messageFromService->prepareDomainObject();
        $messageFrom->setEmailAddress($address);
        $messageFrom->setName($name);
        $message->addMessageFrom($messageFrom);

        return $message;
    }

    public function addReplyTo(
        Message $message,
        string $address,
        ?string $name = null
    ): Message {
        /** @var MessageReplyToService $messageReplyToService */
        $messageReplyToService = $this->applicationServicePluginManager->get(
            MessageReplyToService::class
        );
        /** @var MessageReplyTo $messageReplyTo */
        $messageReplyTo = $messageReplyToService->prepareDomainObject();
        $messageReplyTo->setEmailAddress($address);
        $messageReplyTo->setName($name);
        $message->addMessageReplyTo($messageReplyTo);

        return $message;
    }

    public function addTo(
        Message $message,
        string $address,
        ?string $name = null
    ): Message {
        /** @var MessageToService $messageToService */
        $messageToService = $this->applicationServicePluginManager->get(
            MessageToService::class
        );
        /** @var MessageTo $messageTo */
        $messageTo = $messageToService->prepareDomainObject();
        $messageTo->setEmailAddress($address);
        $messageTo->setName($name);
        $message->addMessageTo($messageTo);

        return $message;
    }

    public function addCc(
        Message $message,
        string $address,
        ?string $name = null
    ): Message {
        /** @var MessageCcService $messageCcService */
        $messageCcService = $this->applicationServicePluginManager->get(
            MessageCcService::class
        );
        /** @var MessageCc $messageCc */
        $messageCc = $messageCcService->prepareDomainObject();
        $messageCc->setEmailAddress($address);
        $messageCc->setName($name);
        $message->addMessageCc($messageCc);

        return $message;
    }

    public function addBcc(
        Message $message,
        string $address,
        ?string $name = null
    ): Message {
        /** @var MessageBccService $messageBccService */
        $messageBccService = $this->applicationServicePluginManager->get(
            MessageBccService::class
        );
        /** @var MessageBcc $messageBcc */
        $messageBcc = $messageBccService->prepareDomainObject();
        $messageBcc->setEmailAddress($address);
        $messageBcc->setName($name);
        $message->addMessageBcc($messageBcc);

        return $message;
    }

    public function send(Message $message): Message
    {
        return $this->transactional(
            function () use ($message) {
                $laminasMessage = new LaminasMessage();

                if ($message->getMessageSubjectTemplate() !== null) {
                    /** @var MessageSubjectTemplateService $messageSubjectTemplateService */
                    $messageSubjectTemplateService = $this->applicationServicePluginManager->get(
                        MessageSubjectTemplateService::class
                    );
                    $message->setSubject(
                        $messageSubjectTemplateService->renderTemplate($message->getMessageSubjectTemplate())
                    );
                }

                /** @var Message $persistedMessage */
                $persistedMessage = $this->create([
                    'messageSubjectTemplate' => $message->getMessageSubjectTemplate(),
                    'subject' => $message->getSubject(),
                ]);

                /** @var MessageContentService $messageContentService */
                $messageContentService = $this->applicationServicePluginManager->get(
                    MessageContentService::class
                );

                foreach ($message->getMessageContentCollection() ?? [] as $messageContent) {
                    $part = new LaminasMessagePart();
                    $part->setType($messageContent->getContentType());
                    $part->setContent($messageContent->getContent());
                    $laminasMessage->addPart($part);

                    $messageContent->setMessage($persistedMessage);
                    $messageContentService->create([], $messageContent);
                    $persistedMessage->addMessageContent($messageContent);
                }

                /** @var MessageAttachmentService $messageAttachmentService */
                $messageAttachmentService = $this->applicationServicePluginManager->get(
                    MessageAttachmentService::class
                );

                foreach ($message->getMessageAttachmentCollection() ?? [] as $messageAttachment) {
                    $part = new LaminasMessagePart();
                    $part->setType($messageAttachment->getContentType());
                    $part->setContent($messageAttachment->getContent());
                    $part->setFileName($messageAttachment->getName());
                    $laminasMessage->addPart($part);

                    $messageAttachment->setMessage($persistedMessage);
                    $messageAttachmentService->create([], $messageAttachment);
                    $persistedMessage->addMessageAttachment($messageAttachment);
                }

                /** @var MessageFromService $messageFromService */
                $messageFromService = $this->applicationServicePluginManager->get(
                    MessageFromService::class
                );
                /** @var MessageReplyToService $messageReplyToService */
                $messageReplyToService = $this->applicationServicePluginManager->get(
                    MessageReplyToService::class
                );
                /** @var MessageToService $messageToService */
                $messageToService = $this->applicationServicePluginManager->get(
                    MessageToService::class
                );
                /** @var MessageCcService $messageCcService */
                $messageCcService = $this->applicationServicePluginManager->get(
                    MessageCcService::class
                );
                /** @var MessageBccService $messageBccService */
                $messageBccService = $this->applicationServicePluginManager->get(
                    MessageBccService::class
                );

                /** @var EmailService $emailInfrastructureService */
                $emailInfrastructureService = $this->infrastructureServicePluginManager->get(
                    EmailService::class
                );
                $emailInfrastructureService->send(
                    $laminasMessage,
                    $message->getSubject(),
                    array_map(
                        function (MessageFrom $from) use ($messageFromService, $persistedMessage): LaminasAddress {
                            $from->setMessage($persistedMessage);
                            $messageFromService->create([], $from);
                            $persistedMessage->addMessageFrom($from);

                            return $from->getAddress();
                        },
                        $message->getMessageFromCollection()
                    ),
                    array_map(
                        function (MessageReplyTo $replyTo) use ($messageReplyToService, $persistedMessage): LaminasAddress {
                            $replyTo->setMessage($persistedMessage);
                            $messageReplyToService->create([], $replyTo);
                            $persistedMessage->addMessageReplyTo($replyTo);

                            return $replyTo->getAddress();
                        },
                        $message->getMessageReplyToCollection()
                    ),
                    array_map(
                        function (MessageTo $to) use ($messageToService, $persistedMessage): LaminasAddress {
                            $to->setMessage($persistedMessage);
                            $messageToService->create([], $to);
                            $persistedMessage->addMessageTo($to);

                            return $to->getAddress();
                        },
                        $message->getMessageToCollection()
                    ),
                    array_map(
                        function (MessageCc $cc) use ($messageCcService, $persistedMessage): LaminasAddress {
                            $cc->setMessage($persistedMessage);
                            $messageCcService->create([], $cc);
                            $persistedMessage->addMessageCc($cc);

                            return $cc->getAddress();
                        },
                        $message->getMessageCcCollection()
                    ),
                    array_map(
                        function (MessageBcc $bcc) use ($messageBccService, $persistedMessage): LaminasAddress {
                            $bcc->setMessage($persistedMessage);
                            $messageBccService->create([], $bcc);
                            $persistedMessage->addMessageBcc($bcc);

                            return $bcc->getAddress();
                        },
                        $message->getMessageBccCollection()
                    )
                );

                return $persistedMessage;
            }
        );
    }

    public function sendFromComponents(
        LaminasMessage|string|null $content = null,
        ?string $subject = null,
        LaminasAddressList|LaminasAddress|iterable $from = [],
        LaminasAddressList|LaminasAddress|iterable $replyTo = [],
        LaminasAddressList|LaminasAddress|iterable $to = [],
        LaminasAddressList|LaminasAddress|iterable $cc = [],
        LaminasAddressList|LaminasAddress|iterable $bcc = []
    ): Message {
        return $this->transactional(
            function () use ($content, $subject, $from, $replyTo, $to, $cc, $bcc) {
                /** @var EmailService $emailInfrastructureService */
                $emailInfrastructureService = $this->infrastructureServicePluginManager->get(
                    EmailService::class
                );
                $emailInfrastructureService->send(
                    $content,
                    $subject,
                    $from,
                    $replyTo,
                    $to,
                    $cc,
                    $bcc,
                );

                $data = [
                    'subject' => $subject,
                ];

                /** @var Message $message */
                $message = $this->create($data);

                /** @var MessageFromService $messageFromService */
                $messageFromService = $this->applicationServicePluginManager->get(MessageFromService::class);
                /** @var MessageFrom[] $entities */
                $entities = [];


                foreach ($from as $address) {
                    $data = [
                        'message' => $message,
                        'emailAddress' => $address->getEmail(),
                        'name' => $address->getName(),
                    ];
                    $entities[] = $messageFromService->create($data);
                }

                $message->setMessageFromCollection($entities);

                /** @var MessageReplyToService $messageReplyToService */
                $messageReplyToService = $this->applicationServicePluginManager->get(MessageReplyToService::class);
                /** @var MessageReplyTo[] $entities */
                $entities = [];

                foreach ($replyTo as $address) {
                    $data = [
                        'message' => $message,
                        'emailAddress' => $address->getEmail(),
                        'name' => $address->getName(),
                    ];
                    $entities[] = $messageReplyToService->create($data);
                }

                $message->setMessageReplyToCollection($entities);

                /** @var MessageToService $messageToService */
                $messageToService = $this->applicationServicePluginManager->get(MessageToService::class);
                /** @var MessageTo[] $entities */
                $entities = [];

                foreach ($to as $address) {
                    $data = [
                        'message' => $message,
                        'emailAddress' => $address->getEmail(),
                        'name' => $address->getName(),
                    ];
                    $entities[] = $messageToService->create($data);
                }

                $message->setMessageToCollection($entities);

                /** @var MessageCcService $messageCcService */
                $messageCcService = $this->applicationServicePluginManager->get(MessageCcService::class);
                /** @var MessageCc[] $entities */
                $entities = [];

                foreach ($cc as $address) {
                    $data = [
                        'message' => $message,
                        'emailAddress' => $address->getEmail(),
                        'name' => $address->getName(),
                    ];
                    $entities[] = $messageCcService->create($data);
                }

                $message->setMessageCcCollection($entities);

                /** @var MessageBccService $messageBccService */
                $messageBccService = $this->applicationServicePluginManager->get(MessageBccService::class);
                /** @var MessageBcc[] $entities */
                $entities = [];

                foreach ($bcc as $address) {
                    $data = [
                        'message' => $message,
                        'emailAddress' => $address->getEmail(),
                        'name' => $address->getName(),
                    ];
                    $entities[] = $messageBccService->create($data);
                }

                $message->setMessageBccCollection($entities);

                return $message;
            }
        );
    }
}
