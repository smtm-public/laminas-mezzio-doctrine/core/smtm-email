<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class MessageHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        //'subject' => 'You must specify a name for the District.',
    ];
}
