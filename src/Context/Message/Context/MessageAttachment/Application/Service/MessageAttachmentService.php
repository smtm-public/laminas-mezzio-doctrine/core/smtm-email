<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageAttachment\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait;
use Smtm\Email\Context\Message\Context\MessageAttachment\Application\Hydrator\MessageAttachmentHydrator;
use Smtm\Email\Context\Message\Context\MessageAttachment\Domain\MessageAttachment;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageAttachmentService extends AbstractDbService
{

    use ArchivableEntityDbServiceTrait;

    protected ?string $domainObjectName = MessageAttachment::class;
    protected ?string $hydratorName = MessageAttachmentHydrator::class;
}
