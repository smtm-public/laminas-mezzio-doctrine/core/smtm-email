<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageAttachment\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class MessageAttachmentHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'name' => 'You must specify a name for the MessageAttachment.',
        'contentType' => 'You must specify a contentType for the MessageAttachment.',
        'content' => 'You must specify a content for the MessageAttachment.',
    ];
}
