<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageAttachment\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageAttachmentRepository extends AbstractRepository implements MessageAttachmentRepositoryInterface
{

}
