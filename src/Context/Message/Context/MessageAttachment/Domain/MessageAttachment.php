<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageAttachment\Domain;

use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;
use Smtm\Email\Context\Message\Domain\Message;
use JetBrains\PhpStorm\Pure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageAttachment implements EntityInterface, NotArchivedAwareEntityInterface, ArchivedDateTimeAwareEntityInterface
{

    use NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;

    protected int $messageId;
    protected Message $message;
    protected string $name;
    protected string $url;
    protected string $contentType;
    protected string $content;

    #[Pure] public function getMessageId(): int
    {
        return $this->message->getId();
    }

    public function getMessage(): Message
    {
        return $this->message;
    }

    public function setMessage(Message $message): static
    {
        $this->message = $message;
        $this->messageId = $message->getId();

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): MessageAttachment
    {
        $this->url = $url;

        return $this;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function setContentType(string $contentType): static
    {
        $this->contentType = $contentType;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;

        return $this;
    }
}
