<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageBcc\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageBccRepository extends AbstractRepository implements MessageBccRepositoryInterface
{

}
