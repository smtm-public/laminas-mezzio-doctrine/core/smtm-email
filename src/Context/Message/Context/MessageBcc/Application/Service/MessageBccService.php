<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageBcc\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait;
use Smtm\Email\Context\Message\Context\MessageBcc\Application\Hydrator\MessageBccHydrator;
use Smtm\Email\Context\Message\Context\MessageBcc\Domain\MessageBcc;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageBccService extends AbstractDbService
{

    use ArchivableEntityDbServiceTrait;

    protected ?string $domainObjectName = MessageBcc::class;
    protected ?string $hydratorName = MessageBccHydrator::class;
}
