<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageBcc\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class MessageBccHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'message' => 'You must specify a message for the MessageBcc.',
        'emailAddress' => 'You must specify an emailAddress for the MessageBcc.',
    ];
}
