<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageTo\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait;
use Smtm\Email\Context\Message\Context\MessageTo\Application\Hydrator\MessageToHydrator;
use Smtm\Email\Context\Message\Context\MessageTo\Domain\MessageTo;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageToService extends AbstractDbService
{

    use ArchivableEntityDbServiceTrait;

    protected ?string $domainObjectName = MessageTo::class;
    protected ?string $hydratorName = MessageToHydrator::class;
}
