<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageTo\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class MessageToHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'message' => 'You must specify a message for the MessageTo.',
        'emailAddress' => 'You must specify an emailAddress for the MessageTo.',
    ];
}
