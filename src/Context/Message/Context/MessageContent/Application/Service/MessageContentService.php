<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageContent\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait;
use Smtm\Email\Context\Message\Context\MessageContent\Application\Hydrator\MessageContentHydrator;
use Smtm\Email\Context\Message\Context\MessageContent\Domain\MessageContent;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageContentService extends AbstractDbService
{

    use ArchivableEntityDbServiceTrait;

    protected ?string $domainObjectName = MessageContent::class;
    protected ?string $hydratorName = MessageContentHydrator::class;
}
