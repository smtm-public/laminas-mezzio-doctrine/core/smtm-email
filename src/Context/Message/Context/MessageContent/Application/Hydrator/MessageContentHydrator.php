<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageContent\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class MessageContentHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'contentType' => 'You must specify a contentType for the MessageContent.',
        'content' => 'You must specify a content for the MessageContent.',
    ];
}
