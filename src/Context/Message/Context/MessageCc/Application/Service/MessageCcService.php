<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageCc\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait;
use Smtm\Email\Context\Message\Context\MessageCc\Application\Hydrator\MessageCcHydrator;
use Smtm\Email\Context\Message\Context\MessageCc\Domain\MessageCc;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageCcService extends AbstractDbService
{

    use ArchivableEntityDbServiceTrait;

    protected ?string $domainObjectName = MessageCc::class;
    protected ?string $hydratorName = MessageCcHydrator::class;
}
