<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageCc\Domain;

use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;
use Smtm\Email\Context\Message\Domain\Message;
use JetBrains\PhpStorm\Pure;
use Laminas\Mail\Address;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageCc implements EntityInterface, NotArchivedAwareEntityInterface, ArchivedDateTimeAwareEntityInterface
{

    use NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;

    protected int $messageId;
    protected Message $message;
    protected ?string $name = null;
    protected string $emailAddress;

    #[Pure] public function getMessageId(): int
    {
        return $this->message->getId();
    }

    public function getMessage(): Message
    {
        return $this->message;
    }

    public function setMessage(Message $message): static
    {
        $this->message = $message;
        $this->messageId = $message->getId();

        return $this;
    }

    public function getAddress(): Address
    {
        return new Address($this->emailAddress, $this->name);
    }

    public function setAddress(Address $address): static
    {
        $this->emailAddress = $address->getEmail();
        $this->name = $address->getName();

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    public function setEmailAddress(string $emailAddress): static
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }
}
