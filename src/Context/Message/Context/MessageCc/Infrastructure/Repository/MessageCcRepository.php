<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageCc\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageCcRepository extends AbstractRepository implements MessageCcRepositoryInterface
{

}
