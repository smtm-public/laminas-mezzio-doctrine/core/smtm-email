<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageCc\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface MessageCcRepositoryInterface extends RepositoryInterface
{

}
