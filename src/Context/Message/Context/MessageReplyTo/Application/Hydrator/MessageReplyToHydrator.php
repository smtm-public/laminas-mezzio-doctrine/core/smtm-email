<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageReplyTo\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class MessageReplyToHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'message' => 'You must specify a message for the MessageReplyTo.',
        'emailAddress' => 'You must specify an emailAddress for the MessageReplyTo.',
    ];
}
