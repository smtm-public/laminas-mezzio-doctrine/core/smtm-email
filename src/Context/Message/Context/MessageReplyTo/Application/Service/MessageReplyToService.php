<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageReplyTo\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait;
use Smtm\Email\Context\Message\Context\MessageReplyTo\Application\Hydrator\MessageReplyToHydrator;
use Smtm\Email\Context\Message\Context\MessageReplyTo\Domain\MessageReplyTo;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageReplyToService extends AbstractDbService
{

    use ArchivableEntityDbServiceTrait;

    protected ?string $domainObjectName = MessageReplyTo::class;
    protected ?string $hydratorName = MessageReplyToHydrator::class;
}
