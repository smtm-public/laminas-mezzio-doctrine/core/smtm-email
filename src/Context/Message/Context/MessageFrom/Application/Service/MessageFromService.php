<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageFrom\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait;
use Smtm\Email\Context\Message\Context\MessageFrom\Application\Hydrator\MessageFromHydrator;
use Smtm\Email\Context\Message\Context\MessageFrom\Domain\MessageFrom;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageFromService extends AbstractDbService
{

    use ArchivableEntityDbServiceTrait;

    protected ?string $domainObjectName = MessageFrom::class;
    protected ?string $hydratorName = MessageFromHydrator::class;
}
