<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Context\MessageFrom\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class MessageFromHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'message' => 'You must specify a message for the MessageCc.',
        'emailAddress' => 'You must specify an emailAddress for the MessageCc.',
    ];
}
