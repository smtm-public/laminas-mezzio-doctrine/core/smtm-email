<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Domain;

use Smtm\Base\Domain\AbstractUuidAwareEntity;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\MarkedForUpdateTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;
use Smtm\Base\MetaProgramming\ArrayOf;
use Smtm\Email\Context\Message\Context\MessageAttachment\Domain\MessageAttachment;
use Smtm\Email\Context\Message\Context\MessageBcc\Domain\MessageBcc;
use Smtm\Email\Context\Message\Context\MessageCc\Domain\MessageCc;
use Smtm\Email\Context\Message\Context\MessageContent\Domain\MessageContent;
use Smtm\Email\Context\Message\Context\MessageFrom\Domain\MessageFrom;
use Smtm\Email\Context\Message\Context\MessageReplyTo\Domain\MessageReplyTo;
use Smtm\Email\Context\Message\Context\MessageTo\Domain\MessageTo;
use Smtm\Email\Context\MessageSubjectTemplate\Domain\MessageSubjectTemplate;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Message extends AbstractUuidAwareEntity implements
    NotArchivedAwareEntityInterface,
    ArchivedDateTimeAwareEntityInterface,
    MarkedForUpdateInterface
{

    use NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait,
        MarkedForUpdateTrait;

    protected ?MessageSubjectTemplate $messageSubjectTemplate = null;
    protected string $subject;
    protected Collection $messageFromCollection;
    protected Collection $messageReplyToCollection;
    protected Collection $messageToCollection;
    protected Collection $messageCcCollection;
    protected Collection $messageBccCollection;
    protected Collection $messageAttachmentCollection;
    protected ?string $contentTemplateParameters = null;
    protected Collection $messageContentCollection;
    protected array $params = [];

    public function __construct()
    {
        parent::__construct();

        $this->messageFromCollection = new ArrayCollection();
        $this->messageReplyToCollection = new ArrayCollection();
        $this->messageToCollection = new ArrayCollection();
        $this->messageCcCollection = new ArrayCollection();
        $this->messageBccCollection = new ArrayCollection();
        $this->messageAttachmentCollection = new ArrayCollection();
        $this->messageContentCollection = new ArrayCollection();
    }

    public function getMessageSubjectTemplate(): ?MessageSubjectTemplate
    {
        return $this->messageSubjectTemplate;
    }

    public function setMessageSubjectTemplate(?MessageSubjectTemplate $messageSubjectTemplate): static
    {
        return $this->__setProperty('messageSubjectTemplate', $messageSubjectTemplate);
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): static
    {
        return $this->__setProperty('subject', $subject);
    }

    /**
     * @return MessageFrom[]
     */
    #[ArrayOf(MessageFrom::class)] public function getMessageFromCollection(): array
    {
        return $this->messageFromCollection->getValues();
    }

    /**
     * @param MessageFrom[] $messageFromArray
     */
    public function setMessageFromCollection(
        #[ArrayOf(MessageFrom::class)] array $messageFromArray
    ): static {
        $messageFromCollection = new ArrayCollection($messageFromArray);

        /** @var MessageFrom $messageFrom */
        foreach ($this->messageFromCollection as $messageFrom) {
            if (!$messageFromCollection->contains($messageFrom)) {
                $oldCollectionValues = $this->messageFromCollection->getValues();
                $this->messageFromCollection->removeElement($messageFrom);
                $newCollectionValues = $this->messageFromCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageFromCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var MessageFrom $messageFrom */
        foreach ($messageFromCollection as $messageFrom) {
            if (!$this->messageFromCollection->contains($messageFrom)) {
                $oldCollectionValues = $this->messageFromCollection->getValues();
                $this->messageFromCollection->add($messageFrom);
                $newCollectionValues = $this->messageFromCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageFromCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function addMessageFrom(MessageFrom $messageFrom): static
    {
        if (!$this->messageFromCollection->contains($messageFrom)) {
            $oldCollectionValues = $this->messageFromCollection->getValues();
            $this->messageFromCollection->add($messageFrom);
            $newCollectionValues = $this->messageFromCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageFromCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function removeMessageFrom(MessageFrom $messageFrom): static
    {
        if ($this->messageFromCollection->contains($messageFrom)) {
            $oldCollectionValues = $this->messageFromCollection->getValues();
            $this->messageFromCollection->removeElement($messageFrom);
            $newCollectionValues = $this->messageFromCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageFromCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function hasMessageFrom(MessageFrom $messageFrom): bool
    {
        return $this->messageFromCollection->contains($messageFrom);
    }

    public function addMessageFromCollection(array $messageFroms): static
    {
        foreach ($messageFroms as $messageFrom) {
            $this->addMessageFrom($messageFrom);
        }

        return $this;
    }

    public function resetMessageFromCollection(): static
    {
        if (!$this->messageFromCollection->isEmpty()) {
            $oldCollectionValues = $this->messageFromCollection->getValues();
            $this->messageFromCollection->clear();
            $newCollectionValues = $this->messageFromCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageFromCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    /**
     * @return MessageReplyTo[]
     */
    #[ArrayOf(MessageReplyTo::class)] public function getMessageReplyToCollection(): array
    {
        return $this->messageReplyToCollection->getValues();
    }

    /**
     * @param MessageReplyTo[] $messageReplyToArray
     */
    public function setMessageReplyToCollection(
        #[ArrayOf(MessageReplyTo::class)] array $messageReplyToArray
    ): static {
        $messageReplyToCollection = new ArrayCollection($messageReplyToArray);

        /** @var MessageReplyTo $messageReplyTo */
        foreach ($this->messageReplyToCollection as $messageReplyTo) {
            if (!$messageReplyToCollection->contains($messageReplyTo)) {
                $oldCollectionValues = $this->messageReplyToCollection->getValues();
                $this->messageReplyToCollection->removeElement($messageReplyTo);
                $newCollectionValues = $this->messageReplyToCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageReplyToCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var MessageReplyTo $messageReplyTo */
        foreach ($messageReplyToCollection as $messageReplyTo) {
            if (!$this->messageReplyToCollection->contains($messageReplyTo)) {
                $oldCollectionValues = $this->messageReplyToCollection->getValues();
                $this->messageReplyToCollection->add($messageReplyTo);
                $newCollectionValues = $this->messageReplyToCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageReplyToCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function addMessageReplyTo(MessageReplyTo $messageReplyTo): static
    {
        if (!$this->messageReplyToCollection->contains($messageReplyTo)) {
            $oldCollectionValues = $this->messageReplyToCollection->getValues();
            $this->messageReplyToCollection->add($messageReplyTo);
            $newCollectionValues = $this->messageReplyToCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageReplyToCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function removeMessageReplyTo(MessageReplyTo $messageReplyTo): static
    {
        if ($this->messageReplyToCollection->contains($messageReplyTo)) {
            $oldCollectionValues = $this->messageReplyToCollection->getValues();
            $this->messageReplyToCollection->removeElement($messageReplyTo);
            $newCollectionValues = $this->messageReplyToCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageReplyToCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function hasMessageReplyTo(MessageReplyTo $messageReplyTo): bool
    {
        return $this->messageReplyToCollection->contains($messageReplyTo);
    }

    public function addMessageReplyToCollection(array $messageReplyTos): static
    {
        foreach ($messageReplyTos as $messageReplyTo) {
            $this->addMessageReplyTo($messageReplyTo);
        }

        return $this;
    }

    public function resetMessageReplyToCollection(): static
    {
        if (!$this->messageReplyToCollection->isEmpty()) {
            $oldCollectionValues = $this->messageReplyToCollection->getValues();
            $this->messageReplyToCollection->clear();
            $newCollectionValues = $this->messageReplyToCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageReplyToCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    /**
     * @return MessageTo[]
     */
    #[ArrayOf(MessageTo::class)] public function getMessageToCollection(): array
    {
        return $this->messageToCollection->getValues();
    }

    /**
     * @param MessageTo[] $messageToArray
     */
    public function setMessageToCollection(
        #[ArrayOf(MessageTo::class)] array $messageToArray
    ): static {
        $messageToCollection = new ArrayCollection($messageToArray);

        /** @var MessageTo $messageTo */
        foreach ($this->messageToCollection as $messageTo) {
            if (!$messageToCollection->contains($messageTo)) {
                $oldCollectionValues = $this->messageToCollection->getValues();
                $this->messageToCollection->removeElement($messageTo);
                $newCollectionValues = $this->messageToCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageToCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var MessageTo $messageTo */
        foreach ($messageToCollection as $messageTo) {
            if (!$this->messageToCollection->contains($messageTo)) {
                $oldCollectionValues = $this->messageToCollection->getValues();
                $this->messageToCollection->add($messageTo);
                $newCollectionValues = $this->messageToCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageToCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function addMessageTo(MessageTo $messageTo): static
    {
        if (!$this->messageToCollection->contains($messageTo)) {
            $oldCollectionValues = $this->messageToCollection->getValues();
            $this->messageToCollection->add($messageTo);
            $newCollectionValues = $this->messageToCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageToCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function removeMessageTo(MessageTo $messageTo): static
    {
        if ($this->messageToCollection->contains($messageTo)) {
            $oldCollectionValues = $this->messageToCollection->getValues();
            $this->messageToCollection->removeElement($messageTo);
            $newCollectionValues = $this->messageToCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageToCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function hasMessageTo(MessageTo $messageTo): bool
    {
        return $this->messageToCollection->contains($messageTo);
    }

    public function addMessageToCollection(array $messageTos): static
    {
        foreach ($messageTos as $messageTo) {
            $this->addMessageTo($messageTo);
        }

        return $this;
    }

    public function resetMessageToCollection(): static
    {
        if (!$this->messageToCollection->isEmpty()) {
            $oldCollectionValues = $this->messageToCollection->getValues();
            $this->messageToCollection->clear();
            $newCollectionValues = $this->messageToCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageToCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    /**
     * @return MessageCc[]
     */
    #[ArrayOf(MessageCc::class)] public function getMessageCcCollection(): array
    {
        return $this->messageCcCollection->getValues();
    }

    /**
     * @param MessageCc[] $messageCcArray
     */
    public function setMessageCcCollection(
        #[ArrayOf(MessageCc::class)] array $messageCcArray
    ): static {
        $messageCcCollection = new ArrayCollection($messageCcArray);

        /** @var MessageCc $messageCc */
        foreach ($this->messageCcCollection as $messageCc) {
            if (!$messageCcCollection->contains($messageCc)) {
                $oldCollectionValues = $this->messageCcCollection->getValues();
                $this->messageCcCollection->removeElement($messageCc);
                $newCollectionValues = $this->messageCcCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageCcCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var MessageCc $messageCc */
        foreach ($messageCcCollection as $messageCc) {
            if (!$this->messageCcCollection->contains($messageCc)) {
                $oldCollectionValues = $this->messageCcCollection->getValues();
                $this->messageCcCollection->add($messageCc);
                $newCollectionValues = $this->messageCcCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageCcCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function addMessageCc(MessageCc $messageCc): static
    {
        if (!$this->messageCcCollection->contains($messageCc)) {
            $oldCollectionValues = $this->messageCcCollection->getValues();
            $this->messageCcCollection->add($messageCc);
            $newCollectionValues = $this->messageCcCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageCcCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function removeMessageCc(MessageCc $messageCc): static
    {
        if ($this->messageCcCollection->contains($messageCc)) {
            $oldCollectionValues = $this->messageCcCollection->getValues();
            $this->messageCcCollection->removeElement($messageCc);
            $newCollectionValues = $this->messageCcCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageCcCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function hasMessageCc(MessageCc $messageCc): bool
    {
        return $this->messageCcCollection->contains($messageCc);
    }

    public function addMessageCcCollection(array $messageCcs): static
    {
        foreach ($messageCcs as $messageCc) {
            $this->addMessageCc($messageCc);
        }

        return $this;
    }

    public function resetMessageCcCollection(): static
    {
        if (!$this->messageCcCollection->isEmpty()) {
            $oldCollectionValues = $this->messageCcCollection->getValues();
            $this->messageCcCollection->clear();
            $newCollectionValues = $this->messageCcCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageCcCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    /**
     * @return MessageBcc[]
     */
    #[ArrayOf(MessageBcc::class)] public function getMessageBccCollection(): array
    {
        return $this->messageBccCollection->getValues();
    }

    /**
     * @param MessageBcc[] $messageBccArray
     */
    public function setMessageBccCollection(
        #[ArrayOf(MessageBcc::class)] array $messageBccArray
    ): static {
        $messageBccCollection = new ArrayCollection($messageBccArray);

        /** @var MessageBcc $messageBcc */
        foreach ($this->messageBccCollection as $messageBcc) {
            if (!$messageBccCollection->contains($messageBcc)) {
                $oldCollectionValues = $this->messageBccCollection->getValues();
                $this->messageBccCollection->removeElement($messageBcc);
                $newCollectionValues = $this->messageBccCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageBccCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var MessageBcc $messageBcc */
        foreach ($messageBccCollection as $messageBcc) {
            if (!$this->messageBccCollection->contains($messageBcc)) {
                $oldCollectionValues = $this->messageBccCollection->getValues();
                $this->messageBccCollection->add($messageBcc);
                $newCollectionValues = $this->messageBccCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageBccCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function addMessageBcc(MessageBcc $messageBcc): static
    {
        if (!$this->messageBccCollection->contains($messageBcc)) {
            $oldCollectionValues = $this->messageBccCollection->getValues();
            $this->messageBccCollection->add($messageBcc);
            $newCollectionValues = $this->messageBccCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageBccCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function removeMessageBcc(MessageBcc $messageBcc): static
    {
        if ($this->messageBccCollection->contains($messageBcc)) {
            $oldCollectionValues = $this->messageBccCollection->getValues();
            $this->messageBccCollection->removeElement($messageBcc);
            $newCollectionValues = $this->messageBccCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageBccCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function hasMessageBcc(MessageBcc $messageBcc): bool
    {
        return $this->messageBccCollection->contains($messageBcc);
    }

    public function addMessageBccCollection(array $messageBccs): static
    {
        foreach ($messageBccs as $messageBcc) {
            $this->addMessageBcc($messageBcc);
        }

        return $this;
    }

    public function resetMessageBccCollection(): static
    {
        if (!$this->messageBccCollection->isEmpty()) {
            $oldCollectionValues = $this->messageBccCollection->getValues();
            $this->messageBccCollection->clear();
            $newCollectionValues = $this->messageBccCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageBccCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    /**
     * @return MessageAttachment[]
     */
    #[ArrayOf(MessageAttachment::class)] public function getMessageAttachmentCollection(): array
    {
        return $this->messageAttachmentCollection->getValues();
    }

    /**
     * @param MessageAttachment[] $messageAttachmentArray
     */
    public function setMessageAttachmentCollection(
        #[ArrayOf(MessageAttachment::class)] array $messageAttachmentArray
    ): static {
        $messageAttachmentCollection = new ArrayCollection($messageAttachmentArray);

        /** @var MessageAttachment $messageAttachment */
        foreach ($this->messageAttachmentCollection as $messageAttachment) {
            if (!$messageAttachmentCollection->contains($messageAttachment)) {
                $oldCollectionValues = $this->messageAttachmentCollection->getValues();
                $this->messageAttachmentCollection->removeElement($messageAttachment);
                $newCollectionValues = $this->messageAttachmentCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageAttachmentCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var MessageAttachment $messageAttachment */
        foreach ($messageAttachmentCollection as $messageAttachment) {
            if (!$this->messageAttachmentCollection->contains($messageAttachment)) {
                $oldCollectionValues = $this->messageAttachmentCollection->getValues();
                $this->messageAttachmentCollection->add($messageAttachment);
                $newCollectionValues = $this->messageAttachmentCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageAttachmentCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function addMessageAttachment(MessageAttachment $messageAttachment): static
    {
        if (!$this->messageAttachmentCollection->contains($messageAttachment)) {
            $oldCollectionValues = $this->messageAttachmentCollection->getValues();
            $this->messageAttachmentCollection->add($messageAttachment);
            $newCollectionValues = $this->messageAttachmentCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageAttachmentCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function removeMessageAttachment(MessageAttachment $messageAttachment): static
    {
        if ($this->messageAttachmentCollection->contains($messageAttachment)) {
            $oldCollectionValues = $this->messageAttachmentCollection->getValues();
            $this->messageAttachmentCollection->removeElement($messageAttachment);
            $newCollectionValues = $this->messageAttachmentCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageAttachmentCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function hasMessageAttachment(MessageAttachment $messageAttachment): bool
    {
        return $this->messageAttachmentCollection->contains($messageAttachment);
    }

    public function addMessageAttachmentCollection(array $messageAttachments): static
    {
        foreach ($messageAttachments as $messageAttachment) {
            $this->addMessageAttachment($messageAttachment);
        }

        return $this;
    }

    public function resetMessageAttachmentCollection(): static
    {
        if (!$this->messageAttachmentCollection->isEmpty()) {
            $oldCollectionValues = $this->messageAttachmentCollection->getValues();
            $this->messageAttachmentCollection->clear();
            $newCollectionValues = $this->messageAttachmentCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageAttachmentCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    /**
     * @return MessageContent[]
     */
    #[ArrayOf(MessageContent::class)] public function getMessageContentCollection(): array
    {
        return $this->messageContentCollection->getValues();
    }

    /**
     * @param MessageContent[] $messageContentArray
     */
    public function setMessageContentCollection(
        #[ArrayOf(MessageContent::class)] array $messageContentArray
    ): static {
        $messageContentCollection = new ArrayCollection($messageContentArray);

        /** @var MessageContent $messageContent */
        foreach ($this->messageContentCollection as $messageContent) {
            if (!$messageContentCollection->contains($messageContent)) {
                $oldCollectionValues = $this->messageContentCollection->getValues();
                $this->messageContentCollection->removeElement($messageContent);
                $newCollectionValues = $this->messageContentCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageContentCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var MessageContent $messageContent */
        foreach ($messageContentCollection as $messageContent) {
            if (!$this->messageContentCollection->contains($messageContent)) {
                $oldCollectionValues = $this->messageContentCollection->getValues();
                $this->messageContentCollection->add($messageContent);
                $newCollectionValues = $this->messageContentCollection->getValues();
                $this->__setMarkedForUpdate(true, 'messageContentCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function addMessageContent(MessageContent $messageContent): static
    {
        if (!$this->messageContentCollection->contains($messageContent)) {
            $oldCollectionValues = $this->messageContentCollection->getValues();
            $this->messageContentCollection->add($messageContent);
            $newCollectionValues = $this->messageContentCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageContentCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function removeMessageContent(MessageContent $messageContent): static
    {
        if ($this->messageContentCollection->contains($messageContent)) {
            $oldCollectionValues = $this->messageContentCollection->getValues();
            $this->messageContentCollection->removeElement($messageContent);
            $newCollectionValues = $this->messageContentCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageContentCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function hasMessageContent(MessageContent $messageContent): bool
    {
        return $this->messageContentCollection->contains($messageContent);
    }

    public function addMessageContentCollection(array $messageContents): static
    {
        foreach ($messageContents as $messageContent) {
            $this->addMessageContent($messageContent);
        }

        return $this;
    }

    public function resetMessageContentCollection(): static
    {
        if (!$this->messageContentCollection->isEmpty()) {
            $oldCollectionValues = $this->messageContentCollection->getValues();
            $this->messageContentCollection->clear();
            $newCollectionValues = $this->messageContentCollection->getValues();
            $this->__setMarkedForUpdate(true, 'messageContentCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function getContentTemplateParameters(): ?string
    {
        return $this->contentTemplateParameters;
    }

    public function setContentTemplateParameters(
        ?string $contentTemplateParameters = null
    ): static {
        $this->contentTemplateParameters = $contentTemplateParameters;

        return $this;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): static
    {
        $this->params = $params;

        return $this;
    }
}
