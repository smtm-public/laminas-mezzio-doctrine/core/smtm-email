<?php

declare(strict_types=1);

namespace Smtm\Email\Context\Message\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface MessageRepositoryInterface extends RepositoryInterface
{

}
