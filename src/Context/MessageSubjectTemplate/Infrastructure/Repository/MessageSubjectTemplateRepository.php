<?php

declare(strict_types=1);

namespace Smtm\Email\Context\MessageSubjectTemplate\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageSubjectTemplateRepository extends AbstractRepository implements MessageSubjectTemplateRepositoryInterface
{

}
