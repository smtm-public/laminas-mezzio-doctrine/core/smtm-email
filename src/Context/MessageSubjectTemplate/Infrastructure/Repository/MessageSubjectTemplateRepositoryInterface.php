<?php

declare(strict_types=1);

namespace Smtm\Email\Context\MessageSubjectTemplate\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface MessageSubjectTemplateRepositoryInterface extends RepositoryInterface
{

}
