<?php

declare(strict_types=1);

namespace Smtm\Email\Context\MessageSubjectTemplate\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\ArchivableUuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Email\Context\MessageSubjectTemplate\Domain\MessageSubjectTemplate;
use JetBrains\PhpStorm\Pure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageSubjectTemplateService extends AbstractDbService implements UuidAwareEntityDbServiceInterface
{

    use ArchivableUuidAwareEntityDbServiceTrait, UuidAwareEntityDbServiceTrait;

    protected ?string $domainObjectName = MessageSubjectTemplate::class;
    protected ?string $hydratorName = null;

    #[Pure] public function renderTemplate(
        MessageSubjectTemplate $messageSubjectTemplate
    ): string {
        return strtr($messageSubjectTemplate->getTemplate(), $messageSubjectTemplate->getParams());
    }
}
