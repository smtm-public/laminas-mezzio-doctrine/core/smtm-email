<?php

declare(strict_types=1);

namespace Smtm\Email;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'dependencies' => 'array',
        'doctrine' => 'array',
        'email' => 'array'
    ])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'doctrine' => include __DIR__ . '/../config/doctrine.php',
            'email' => include __DIR__ . '/../config/email.php',
        ];
    }
}
