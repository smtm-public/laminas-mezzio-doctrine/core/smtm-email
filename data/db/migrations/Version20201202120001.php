<?php

declare(strict_types=1);

namespace Smtm\Email\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120001 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        //$this->populateAuthRoleTable($schema);
    }

    public function populateAuthRoleTable(Schema $schema): void
    {
        /*
        $this->connection->insert('auth_role', [
            'id' => 1,
            'uuid' => '1eece020-713e-4eff-9102-1c9ccf884b96',
            'code' => 'user',
            'created' => date('Y-m-d H:i:s'),
            'not_archived' => 1,
        ]);
        $this->connection->insert('auth_role', [
            'id' => 2,
            'uuid' => '64479bfc-c7ba-499e-b9c6-70039361fcc9',
            'code' => 'admin',
            'created' => date('Y-m-d H:i:s'),
            'not_archived' => 1,
        ]);
        */
    }

    public function down(Schema $schema): void
    {

    }
}
