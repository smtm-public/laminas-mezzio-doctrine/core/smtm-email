<?php

declare(strict_types=1);

namespace Smtm\Email\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120000 extends AbstractMigration
{

    use CommonMigrationTrait;

    public function up(Schema $schema): void
    {
        $this->createEmailMessageTable($schema);
        $this->createEmailMessageSubjectTemplateTable($schema);
        $this->createEmailMessageFromTable($schema);
        $this->createEmailMessageReplyToTable($schema);
        $this->createEmailMessageToTable($schema);
        $this->createEmailMessageCcTable($schema);
        $this->createEmailMessageBccTable($schema);
        $this->createEmailMessageAttachmentTable($schema);
        $this->createEmailMessageContentTemplateTable($schema);
        $this->createEmailMessageContentTable($schema);
    }

    public function createEmailMessageTable(Schema $schema): void
    {
        $emailMessageTable = $schema->createTable('email_message');
        $emailMessageTable->addColumn('id', Types::INTEGER, ['notNull' => true])
            ->setAutoincrement(true);
        $emailMessageTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($emailMessageTable);
        $emailMessageTable->addColumn(
            'email_message_subject_template_id',
            Types::INTEGER,
            ['notNull' => false]
        );
        $emailMessageTable->addIndex(
            ['email_message_subject_template_id'],
            substr(
                'idx_' . $emailMessageTable->getName() . '_email_message_subject_template_id',
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
        $emailMessageTable->addForeignKeyConstraint(
            'email_message_subject_template',
            ['email_message_subject_template_id'],
            ['id'],
            [],
            'fk_' . $emailMessageTable->getName() . '_email_message_subject_template_id'
        );
        $emailMessageTable->addColumn('subject', Types::TEXT, ['notNull' => false]);
        $emailMessageTable->addColumn(
            'content_template_parameters',
            Types::TEXT,
            ['notNull' => false]
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($emailMessageTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($emailMessageTable);
        $this->addNotArchivedSmallintColumnAndIndex($emailMessageTable);
    }

    public function createEmailMessageSubjectTemplateTable(Schema $schema): void
    {
        $emailMessageSubjectTemplateTable = $schema->createTable('email_message_subject_template');
        $emailMessageSubjectTemplateTable->addColumn('id', Types::INTEGER, ['notNull' => true])
            ->setAutoincrement(true);
        $emailMessageSubjectTemplateTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($emailMessageSubjectTemplateTable);
        $emailMessageSubjectTemplateTable->addColumn(
            'r_name',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageSubjectTemplateTable->addUniqueIndex(
            ['r_name'],
            substr(
                'idx_unq_' . $emailMessageSubjectTemplateTable->getName() . '_email_message_subject_template_r_name',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $emailMessageSubjectTemplateTable->addColumn(
            'r_template',
            Types::TEXT,
            ['notNull' => true]
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($emailMessageSubjectTemplateTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($emailMessageSubjectTemplateTable);
        $this->addNotArchivedSmallintColumnAndIndex($emailMessageSubjectTemplateTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($emailMessageSubjectTemplateTable);
        $emailMessageSubjectTemplateTable->addUniqueIndex(
            ['r_name', 'not_archived'],
            substr(
                'idx_unq_' . $emailMessageSubjectTemplateTable->getName() . '_r_name_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
    }

    public function createEmailMessageFromTable(Schema $schema): void
    {
        $emailMessageFromTable = $schema->createTable('email_message_from');
        $emailMessageFromTable->addColumn(
            'email_message_id',
            Types::INTEGER,
            ['notNull' => true]
        );
        $emailMessageFromTable->addIndex(
            ['email_message_id'],
            'idx_' . $emailMessageFromTable->getName() . '_email_message_id'
        );
        $emailMessageFromTable->addForeignKeyConstraint(
            'email_message',
            ['email_message_id'],
            ['id'],
            [],
            'fk_' . $emailMessageFromTable->getName() . '_email_message_id'
        );
        $emailMessageFromTable->addColumn(
            'r_name',
            Types::STRING,
            ['length' => 255, 'notNull' => false]
        );
        $emailMessageFromTable->addColumn(
            'email_address',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageFromTable->setPrimaryKey(['email_message_id', 'email_address']);
        $emailMessageFromTable->addUniqueIndex(
            ['email_message_id', 'email_address'],
            substr(
                'idx_unq_' . $emailMessageFromTable->getName() .  '_email_message_id_email_address',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($emailMessageFromTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($emailMessageFromTable);
        $this->addNotArchivedSmallintColumnAndIndex($emailMessageFromTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($emailMessageFromTable);
    }

    public function createEmailMessageReplyToTable(Schema $schema): void
    {
        $emailMessageReplyToTable = $schema->createTable('email_message_reply_to');
        $emailMessageReplyToTable->addColumn(
            'email_message_id',
            Types::INTEGER,
            ['notNull' => true]
        );
        $emailMessageReplyToTable->addIndex(
            ['email_message_id'],
            'idx_' . $emailMessageReplyToTable->getName() . '_email_message_id'
        );
        $emailMessageReplyToTable->addForeignKeyConstraint(
            'email_message',
            ['email_message_id'],
            ['id'],
            [],
            'fk_' . $emailMessageReplyToTable->getName() . '_email_message_id'
        );
        $emailMessageReplyToTable->addColumn(
            'r_name',
            Types::STRING,
            ['length' => 255, 'notNull' => false]
        );
        $emailMessageReplyToTable->addColumn(
            'email_address',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageReplyToTable->setPrimaryKey(['email_message_id', 'email_address']);
        $emailMessageReplyToTable->addUniqueIndex(
            ['email_message_id', 'email_address'],
            substr(
                'idx_unq_' . $emailMessageReplyToTable->getName() .  '_email_message_id_email_address',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($emailMessageReplyToTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($emailMessageReplyToTable);
        $this->addNotArchivedSmallintColumnAndIndex($emailMessageReplyToTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($emailMessageReplyToTable);
    }

    public function createEmailMessageToTable(Schema $schema): void
    {
        $emailMessageToTable = $schema->createTable('email_message_to');
        $emailMessageToTable->addColumn(
            'email_message_id',
            Types::INTEGER,
            ['notNull' => true]
        );
        $emailMessageToTable->addIndex(
            ['email_message_id'],
            'idx_' . $emailMessageToTable->getName() . '_email_message_id'
        );
        $emailMessageToTable->addForeignKeyConstraint(
            'email_message',
            ['email_message_id'],
            ['id'],
            [],
            'fk_' . $emailMessageToTable->getName() . '_email_message_id'
        );
        $emailMessageToTable->addColumn(
            'r_name',
            Types::STRING,
            ['length' => 255, 'notNull' => false]
        );
        $emailMessageToTable->addColumn(
            'email_address',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageToTable->setPrimaryKey(['email_message_id', 'email_address']);
        $emailMessageToTable->addUniqueIndex(
            ['email_message_id', 'email_address'],
            substr(
                'idx_unq_' . $emailMessageToTable->getName() .  '_email_message_id_email_address',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($emailMessageToTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($emailMessageToTable);
        $this->addNotArchivedSmallintColumnAndIndex($emailMessageToTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($emailMessageToTable);
    }

    public function createEmailMessageCcTable(Schema $schema): void
    {
        $emailMessageCcTable = $schema->createTable('email_message_cc');
        $emailMessageCcTable->addColumn(
            'email_message_id',
            Types::INTEGER,
            ['notNull' => true]
        );
        $emailMessageCcTable->addIndex(
            ['email_message_id'],
            'idx_' . $emailMessageCcTable->getName() . '_email_message_id'
        );
        $emailMessageCcTable->addForeignKeyConstraint(
            'email_message',
            ['email_message_id'],
            ['id'],
            [],
            'fk_' . $emailMessageCcTable->getName() . '_email_message_id'
        );
        $emailMessageCcTable->addColumn(
            'r_name',
            Types::STRING,
            ['length' => 255, 'notNull' => false]
        );
        $emailMessageCcTable->addColumn(
            'email_address',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageCcTable->setPrimaryKey(['email_message_id', 'email_address']);
        $emailMessageCcTable->addUniqueIndex(
            ['email_message_id', 'email_address'],
            substr(
                'idx_unq_' . $emailMessageCcTable->getName() .  '_email_message_id_email_address',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($emailMessageCcTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($emailMessageCcTable);
        $this->addNotArchivedSmallintColumnAndIndex($emailMessageCcTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($emailMessageCcTable);
    }

    public function createEmailMessageBccTable(Schema $schema): void
    {
        $emailMessageBccTable = $schema->createTable('email_message_bcc');
        $emailMessageBccTable->addColumn(
            'email_message_id',
            Types::INTEGER,
            ['notNull' => true]
        );
        $emailMessageBccTable->addIndex(
            ['email_message_id'],
            'idx_' . $emailMessageBccTable->getName() . '_email_message_id'
        );
        $emailMessageBccTable->addForeignKeyConstraint(
            'email_message',
            ['email_message_id'],
            ['id'],
            [],
            'fk_' . $emailMessageBccTable->getName() . '_email_message_id'
        );
        $emailMessageBccTable->addColumn(
            'r_name',
            Types::STRING,
            ['length' => 255, 'notNull' => false]
        );
        $emailMessageBccTable->addColumn(
            'email_address',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageBccTable->setPrimaryKey(['email_message_id', 'email_address']);
        $emailMessageBccTable->addUniqueIndex(
            ['email_message_id', 'email_address'],
            substr(
                'idx_unq_' . $emailMessageBccTable->getName() .  '_email_message_id_email_address',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($emailMessageBccTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($emailMessageBccTable);
        $this->addNotArchivedSmallintColumnAndIndex($emailMessageBccTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($emailMessageBccTable);
    }

    public function createEmailMessageAttachmentTable(Schema $schema): void
    {
        $emailMessageAttachmentTable = $schema->createTable('email_message_attachment');
        $emailMessageAttachmentTable->addColumn(
            'email_message_id',
            Types::INTEGER,
            ['notNull' => true]
        );
        $emailMessageAttachmentTable->addIndex(
            ['email_message_id'],
            'idx_' . $emailMessageAttachmentTable->getName() . '_email_message_id'
        );
        $emailMessageAttachmentTable->addForeignKeyConstraint(
            'email_message',
            ['email_message_id'],
            ['id'],
            [],
            'fk_' . $emailMessageAttachmentTable->getName() . '_email_message_id'
        );
        $emailMessageAttachmentTable->addColumn(
            'r_name',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageAttachmentTable->addColumn(
            'content_type',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageAttachmentTable->addColumn(
            'content',
            Types::TEXT,
            ['notNull' => true]
        );
        $emailMessageAttachmentTable->setPrimaryKey(['email_message_id', 'r_name']);
        $emailMessageAttachmentTable->addUniqueIndex(
            ['email_message_id', 'r_name'],
            substr(
                'idx_unq_' . $emailMessageAttachmentTable->getName() . '_email_message_id_r_name',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($emailMessageAttachmentTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($emailMessageAttachmentTable);
        $this->addNotArchivedSmallintColumnAndIndex($emailMessageAttachmentTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($emailMessageAttachmentTable);
    }

    public function createEmailMessageContentTemplateTable(Schema $schema): void
    {
        $emailMessageContentTemplateTable = $schema->createTable('email_message_content_template');
        $emailMessageContentTemplateTable->addColumn('id', Types::INTEGER, ['notNull' => true])
            ->setAutoincrement(true);
        $emailMessageContentTemplateTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($emailMessageContentTemplateTable);
        $emailMessageContentTemplateTable->addColumn(
            'r_name',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageContentTemplateTable->addColumn(
            'content_type',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageContentTemplateTable->addColumn(
            'r_template',
            Types::TEXT,
            ['notNull' => false]
        );
        $emailMessageContentTemplateTable->addColumn(
            'file_template',
            Types::STRING,
            ['length' => 255, 'notNull' => false]
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($emailMessageContentTemplateTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($emailMessageContentTemplateTable);
        $this->addNotArchivedSmallintColumnAndIndex($emailMessageContentTemplateTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($emailMessageContentTemplateTable);
        $emailMessageContentTemplateTable->addUniqueIndex(
            ['r_name', 'content_type', 'not_archived'],
            substr(
                'idx_unq_' . $emailMessageContentTemplateTable->getName() . '_r_name_context_type_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
    }

    public function createEmailMessageContentTable(Schema $schema): void
    {
        $emailMessageContentTable = $schema->createTable('email_message_content');
        $emailMessageContentTable->addColumn(
            'email_message_id',
            Types::INTEGER,
            ['notNull' => true]
        );
        $emailMessageContentTable->addIndex(
            ['email_message_id'],
            'idx_' . $emailMessageContentTable->getName() . '_email_message_id'
        );
        $emailMessageContentTable->addForeignKeyConstraint(
            'email_message',
            ['email_message_id'],
            ['id'],
            [],
            'fk_' . $emailMessageContentTable->getName() . '_email_message_id'
        );
        $emailMessageContentTable->addColumn(
            'content_type',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $emailMessageContentTable->addColumn(
            'email_message_content_template_id',
            Types::INTEGER,
            ['notNull' => false]
        );
        $emailMessageContentTable->addIndex(
            ['email_message_content_template_id'],
            'idx_email_message_content_template_id'
        );
        $emailMessageContentTable->addForeignKeyConstraint(
            'email_message_content_template',
            ['email_message_content_template_id'],
            ['id'],
            [],
            'fk_' . $emailMessageContentTable->getName() . '_email_message_content_template_id'
        );
        $emailMessageContentTable->addColumn(
            'content',
            Types::TEXT,
            ['notNull' => true]
        );
        $emailMessageContentTable->setPrimaryKey(['email_message_id', 'content_type']);
        $emailMessageContentTable->addUniqueIndex(
            ['email_message_id', 'content_type'],
            substr(
                'idx_unq_' . $emailMessageContentTable->getName() . '_email_message_id_content_type',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($emailMessageContentTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($emailMessageContentTable);
        $this->addNotArchivedSmallintColumnAndIndex($emailMessageContentTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($emailMessageContentTable);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('email_message_content');
        $schema->dropTable('email_message_content_template');
        $schema->dropTable('email_message_attachment');
        $schema->dropTable('email_message_bcc');
        $schema->dropTable('email_message_cc');
        $schema->dropTable('email_message_to');
        $schema->dropTable('email_message_reply_to');
        $schema->dropTable('email_message_from');
        $schema->dropTable('email_message_subject_template');
        $schema->dropTable('email_message');
    }
}
