<?php

declare(strict_types=1);

namespace Smtm\Email;

use Smtm\Base\Infrastructure\Helper\EnvHelper;
use Laminas\Mail\Transport\Smtp;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-email')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-email'
    );
    $dotenv->load();
}

$emailProcessorConnectionConfig = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_EMAIL_PROCESSOR_SMTP_CONNECTION_CONFIG'),
    true,
    flags: JSON_THROW_ON_ERROR
);

return [
    'emailProcessor' => [
        'transport' =>
            EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_EMAIL_PROCESSOR_TRANSPORT'),
        Smtp::class => [
            'connectionClass' =>
                EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_EMAIL_PROCESSOR_SMTP_CONNECTION_CLASS'),
            'connectionConfig' =>
                $emailProcessorConnectionConfig,
            'name' =>
                EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_EMAIL_PROCESSOR_SMTP_NAME'),
            'host' =>
                EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_EMAIL_PROCESSOR_SMTP_HOST'),
            'port' =>
                EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_EMAIL_PROCESSOR_SMTP_PORT'),
            'connectionTimeLimit' =>
                EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_EMAIL_PROCESSOR_SMTP_CONNECTION_TIME_LIMIT', null),
        ],
    ],
    'allowedNonProductionDomains' =>
        explode(
            ',',
            EnvHelper::getEnvFromProcessOrSuperGlobal(
                ['SMTM_EMAIL_PROCESSOR_ALLOWED_NONPRODUCTION_DOMAINS', 'EMAIL_PROCESSOR_ALLOWED_NONPRODUCTION_DOMAINS']
            )
        ),
];
