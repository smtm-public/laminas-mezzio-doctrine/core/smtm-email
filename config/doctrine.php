<?php

declare(strict_types=1);

namespace Smtm\Email;

return [
    'orm' => [
        'mapping' => [
            'paths' => [
                __DIR__ . '/../src/Context/Message/Context/MessageAttachment/Infrastructure/Doctrine/Orm/Mapping',
                __DIR__ . '/../src/Context/Message/Context/MessageBcc/Infrastructure/Doctrine/Orm/Mapping',
                __DIR__ . '/../src/Context/Message/Context/MessageCc/Infrastructure/Doctrine/Orm/Mapping',
                __DIR__ . '/../src/Context/Message/Context/MessageContent/Infrastructure/Doctrine/Orm/Mapping',
                __DIR__ . '/../src/Context/Message/Context/MessageFrom/Infrastructure/Doctrine/Orm/Mapping',
                __DIR__ . '/../src/Context/Message/Context/MessageReplyTo/Infrastructure/Doctrine/Orm/Mapping',
                __DIR__ . '/../src/Context/Message/Context/MessageTo/Infrastructure/Doctrine/Orm/Mapping',
                __DIR__ . '/../src/Context/Message/Infrastructure/Doctrine/Orm/Mapping',
                __DIR__ . '/../src/Context/MessageContentTemplate/Infrastructure/Doctrine/Orm/Mapping',
                __DIR__ . '/../src/Context/MessageSubjectTemplate/Infrastructure/Doctrine/Orm/Mapping',
            ],
        ],
    ],
];
