<?php

declare(strict_types=1);

namespace Smtm\Email;

use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Email\Context\MessageContentTemplate\Application\Service\Factory\MessageContentTemplateServiceFactory;
use Smtm\Email\Context\MessageContentTemplate\Application\Service\MessageContentTemplateService;
use Smtm\Email\Infrastructure\Service\EmailService;
use Smtm\Email\Infrastructure\Service\Factory\EmailConfigAwareServiceFactory;
use Smtm\Email\Infrastructure\Service\Factory\EmailRecipientDomainFilterFactory;
use Smtm\Email\Infrastructure\Service\EmailRecipientDomainFilter;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        InfrastructureServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $callback();

                return $infrastructureServicePluginManager->configure(
                    [
                        'factories' => [
                            EmailService::class => EmailConfigAwareServiceFactory::class,
                            EmailRecipientDomainFilter::class => EmailRecipientDomainFilterFactory::class
                        ],
                    ]
                );
            }
        ],
        ApplicationServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var ApplicationServicePluginManager $applicationServicePluginManager */
                $applicationServicePluginManager = $callback();

                return $applicationServicePluginManager->configure(
                    [
                        'factories' => [
                            MessageContentTemplateService::class => MessageContentTemplateServiceFactory::class,
                        ],
                    ]
                );
            }
        ],
    ],
];
