<?php

declare(strict_types=1);

namespace Smtm\Email;

return [
    'em' => 'default',

    'table_storage' => [
        'table_name' => 'doctrine_migrations_smtm_email',
    ],

    'migrations_paths' => [
        'Smtm\Email\Migration' => __DIR__ . '/../data/db/migrations',
    ],
];
